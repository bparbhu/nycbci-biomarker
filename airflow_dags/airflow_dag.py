from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta

# Replace with your actual imports
from src.kedro_streamz.pipelines.preprocessing.nodes import (
    read_edf_store_bigtable,
    dask_read_parquet_upload_bigquery,
    bigtable_to_dataframe_to_parquet,
    upload_parquet_to_bigquery,
)

from src.kedro_streamz.pipelines.tsfresh_features.nodes import (
    burst_detection,
    extract_burst_features,
    send_to_feature_store,
    boruta_feature_selection_and_store,
    extract_time_series_features,
    stream_extract_features,
    store_features_bigquery,
    store_features_redshift,
    store_features_dask_parquet,
    dask_parquet_to_bigquery,
)

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 3, 15),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    'your_kedro_airflow_dag',
    default_args=default_args,
    description='A comprehensive DAG with Kedro nodes',
    schedule_interval=timedelta(days=1),
)

# Define Airflow tasks
t1 = PythonOperator(
    task_id='read_edf_store_bigtable',
    python_callable=read_edf_store_bigtable,
    dag=dag,
)

t2 = PythonOperator(
    task_id='dask_read_parquet_upload_bigquery',
    python_callable=dask_read_parquet_upload_bigquery,
    dag=dag,
)

t3 = PythonOperator(
    task_id='bigtable_to_dataframe_to_parquet',
    python_callable=bigtable_to_dataframe_to_parquet,
    dag=dag,
)

t4 = PythonOperator(
    task_id='upload_parquet_to_bigquery',
    python_callable=upload_parquet_to_bigquery,
    dag=dag,
)

t5 = PythonOperator(
    task_id='extract_time_series_features',
    python_callable=extract_time_series_features,
    dag=dag,
)

t6 = PythonOperator(
    task_id='extract_burst_features',
    python_callable=extract_burst_features,
    dag=dag,
)

t7 = PythonOperator(
    task_id='send_to_feature_store',
    python_callable=send_to_feature_store,
    dag=dag,
)

t8 = PythonOperator(
    task_id='boruta_feature_selection_and_store',
    python_callable=boruta_feature_selection_and_store,
    dag=dag,
)

t9 = PythonOperator(
    task_id='store_features_bigquery',
    python_callable=store_features_bigquery,
    dag=dag,
)

t10 = PythonOperator(
    task_id='store_features_redshift',
    python_callable=store_features_redshift,
    dag=dag,
)

t11 = PythonOperator(
    task_id='store_features_dask_parquet',
    python_callable=store_features_dask_parquet,
    dag=dag,
)

t12 = PythonOperator(
    task_id='dask_parquet_to_bigquery',
    python_callable=dask_parquet_to_bigquery,
    dag=dag,
)

# Set task dependencies
#t1 >> t2 >> t3 >> t4 >> t5 >> t6 >> t7 >> t8 >> t9 >> t10 >> t11 >> t12
