from src.kedro_streamz.pipelines.tsfresh_features.pipeline import create_pipeline
from src.kedro_streamz.runners.state_runner import StateRunner
from kedro.framework.context import KedroContext

context = KedroContext(package_name="nycbci-biomarker")
pipeline = create_pipeline()
catalog = context.catalog
run_id = "test_1"  # Optional

state_runner = StateRunner()
state_runner.run(pipeline, catalog, run_id)
