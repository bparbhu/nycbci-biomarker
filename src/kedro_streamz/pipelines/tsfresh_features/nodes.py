from kedro.io import DataCatalog
import pandas as pd
from tsfresh import extract_features
from tsfresh.utilities.dataframe_functions import roll_time_series
from dask import dataframe as dd
from streamz import Stream
from sqlalchemy import create_engine
from google.cloud import bigquery
from dask import delayed
from google.cloud import aiplatform
from boruta import BorutaPy
from sklearn.ensemble import RandomForestClassifier
import numpy as np


def burst_detection(signal, threshold, min_duration):
    """
    Detect bursts in a 1D array (signal) based on amplitude threshold and minimum duration.
    :param signal: 1D NumPy array representing the signal
    :param threshold: Amplitude threshold for burst detection
    :param min_duration: Minimum duration of bursts
    :return: DataFrame containing information about each detected burst
    """
    above_threshold = np.where(signal > threshold)[0]
    if len(above_threshold) == 0:
        return pd.DataFrame(columns=["start_index", "end_index", "duration", "max_amplitude"])

    bursts = []
    start_idx = above_threshold[0]

    for i in range(1, len(above_threshold)):
        if above_threshold[i] != above_threshold[i-1] + 1:
            end_idx = above_threshold[i-1]
            if end_idx - start_idx >= min_duration:
                duration = end_idx - start_idx + 1
                max_amplitude = np.max(signal[start_idx:end_idx+1])
                bursts.append((start_idx, end_idx, duration, max_amplitude))
            start_idx = above_threshold[i]

    end_idx = above_threshold[-1]
    if end_idx - start_idx >= min_duration:
        duration = end_idx - start_idx + 1
        max_amplitude = np.max(signal[start_idx:end_idx+1])
        bursts.append((start_idx, end_idx, duration, max_amplitude))

    return pd.DataFrame(bursts, columns=["start_index", "end_index", "duration", "max_amplitude"])


def extract_burst_features(df: pd.DataFrame, threshold=2, min_duration=2) -> pd.DataFrame:
    """
    A wrapper function to apply burst detection to a DataFrame
    """
    burst_dfs = []
    for idx, row in df.iterrows():
        signal = row['signal']
        burst_df = burst_detection(signal, threshold, min_duration)
        burst_df['signal_id'] = idx  # Add an identifier for each signal
        burst_dfs.append(burst_df)

    return pd.concat(burst_dfs, ignore_index=True)


def send_to_feature_store():

    # Initialize Vertex AI Feature Store
    aiplatform.init(project="your-gcp-project", location="us-central1")
    featurestore = aiplatform.Featurestore("your-featurestore-name")

    # Initialize BigQuery client
    bq_client = bigquery.Client()

    # Write your SQL query to fetch the features
    sql_query = "SELECT * FROM `your-gcp-project.your_dataset.your_table`"

    # Run the query and get a pandas DataFrame
    df = bq_client.query(sql_query).to_dataframe()

    # Create SQLAlchemy Engine for Vertex AI Feature Store
    # You would typically have this info in your kedro catalog.yml
    engine = create_engine("your_vertex_ai_connection_string")

    # Write data to feature store
    df.to_sql("your_featurestore_table", engine)



def boruta_feature_selection_and_store(features_df, y):
    # Initialize Random Forest classifier
    rf = RandomForestClassifier(n_jobs=-1, class_weight='balanced', max_depth=5)

    # Initialize Boruta
    boruta_selector = BorutaPy(
        estimator=rf,
        n_estimators='auto',
        verbose=2,
        random_state=1,
    )

    # Fit Boruta
    boruta_selector.fit(features_df.values, y.values)

    # Get the selected features
    selected_features = features_df.columns[boruta_selector.support_]

    # Create a DataFrame with only the selected features
    selected_features_df = features_df[selected_features]

    # Save to a parquet file
    parquet_file_path = 'selected_features.parquet'
    selected_features_df.to_parquet(parquet_file_path)

    # Upload to BigQuery using Dask
    dask_df = dd.from_pandas(selected_features_df, npartitions=5)
    dask_df.to_parquet('gs://your_bigquery_bucket/selected_features.parquet')

    return selected_features


def extract_time_series_features(catalog: DataCatalog) -> pd.DataFrame:
    """
    Extract features from time series data using tsfresh.

    Args:
        catalog: Kedro DataCatalog to load the input data.

    Returns:
        A DataFrame with extracted features.
    """
    df = catalog.load("raw_time_series")  # Load the DataFrame from the catalog

    # Convert pandas dataframe to Dask dataframe.
    dask_df = dd.from_pandas(df, npartitions=4)

    # Rolling the dataframe for window-based features.
    rolled_df = roll_time_series(dask_df.compute(), column_id="id", column_sort="time")

    # Using tsfresh to extract features.
    features = extract_features(rolled_df, column_id="id", column_sort="time")

    return features


def stream_extract_features(catalog: DataCatalog) -> Stream:
    """
    Extract features from streaming time series data using tsfresh and Streamz.

    Args:
        catalog: Kedro DataCatalog to load the input data.

    Returns:
        Streamz data stream with extracted features.
    """
    source = Stream(scatter=True, asynchronous=True)
    data_stream = source.map(catalog.load("raw_time_series"))
    processed_stream = data_stream.map(extract_time_series_features)

    return processed_stream  # You can also return the source if needed to feed in data from external sources


def store_features_bigquery(features: pd.DataFrame) -> None:
    """
    Store the features DataFrame into a BigQuery table.

    Args:
        features: DataFrame containing tsfresh features.

    Returns:
        None
    """
    # Assuming you've set up GOOGLE_APPLICATION_CREDENTIALS
    features.to_gbq('your_project_id.your_dataset.your_table_name')

def store_features_redshift(features: pd.DataFrame) -> None:
    """
    Store the features DataFrame into a Redshift table.

    Args:
        features: DataFrame containing tsfresh features.

    Returns:
        None
    """
    engine = create_engine('postgresql+psycopg2://username:password@redshift-hostname:port/database')
    features.to_sql('your_table_name', engine, if_exists='replace', index=False)


def store_features_dask_parquet(features: pd.DataFrame, parquet_path: str) -> None:
    """
    Store the features DataFrame into a Parquet file using Dask.

    Args:
        features: DataFrame containing tsfresh features.
        parquet_path: The file path where to store the Parquet file.

    Returns:
        None
    """
    dask_df = dd.from_pandas(features, npartitions=4)
    dask_df.to_parquet(parquet_path)

@delayed
def dask_parquet_to_bigquery(parquet_path: str, bigquery_project_id: str, dataset_name: str, table_name: str) -> None:
    """
    Upload data from a Parquet file to BigQuery using Dask.

    Args:
        parquet_path: The file path where the Parquet file is stored.
        bigquery_project_id: The BigQuery project id.
        dataset_name: The BigQuery dataset name.
        table_name: The BigQuery table name.

    Returns:
        None
    """
    # Initialize BigQuery client
    bigquery_client = bigquery.Client(project=bigquery_project_id)
    dataset_ref = bigquery_client.dataset(dataset_name)
    table_ref = dataset_ref.table(table_name)

    # Create a job to upload the Parquet file to BigQuery
    job_config = bigquery.LoadJobConfig(source_format=bigquery.SourceFormat.PARQUET)
    with open(parquet_path, 'rb') as parquet_file:
        job = bigquery_client.load_table_from_file(parquet_file, table_ref, job_config=job_config)

    job.result() # Wait for the job to complete