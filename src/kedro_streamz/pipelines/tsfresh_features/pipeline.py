from kedro.pipeline import Pipeline, node
from src.kedro_streamz.pipelines.preprocessing.nodes import (
    read_edf_store_bigtable,
    dask_read_parquet_upload_bigquery,
    bigtable_to_dataframe_to_parquet,
    upload_parquet_to_bigquery,
)
from .nodes import (
    extract_time_series_features,
    stream_extract_features,
    store_features_redshift,
    store_features_bigquery,
    send_to_feature_store,
    boruta_feature_selection_and_store,  # Importing the new node for Boruta feature selection
)

def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                read_edf_store_bigtable,
                None,
                "bigtable_data",
                name="read_edf_store_bigtable",
                tags=["subpipeline_1"],
            ),
            node(
                bigtable_to_dataframe_to_parquet,
                "bigtable_data",
                "parquet_file_path",
                name="bigtable_to_dataframe_to_parquet",
                tags=["subpipeline_2"],
            ),
            node(
                dask_read_parquet_upload_bigquery,
                "parquet_file_path",
                "bigquery_raw_data",
                name="dask_read_parquet_upload_bigquery",
                tags=["subpipeline_2"],
            ),
            node(
                extract_time_series_features,
                "bigquery_raw_data",
                "features",
                name="tsfresh_calculation",
                tags=["subpipeline_3"],
            ),
            node(
                store_features_redshift,
                "features",
                None,
                name="store_redshift",
                tags=["subpipeline_3"],
            ),
            node(
                store_features_bigquery,
                "features",
                None,
                name="store_bigquery",
                tags=["subpipeline_3"],
            ),
            node(
                boruta_feature_selection_and_store,  # Adding the new node for Boruta before send_to_feature_store
                ["features", "y"],  # Assuming "y" is your target variable DataSet in Kedro
                "selected_features",  # Output of the Boruta feature selection, will be input for the next node
                name="boruta_feature_selection_and_store",
                tags=["subpipeline_4"],
            ),
            node(
                send_to_feature_store,
                "selected_features",  # Input is now the selected features from Boruta
                None,
                name="send_to_feature_store",
                tags=["subpipeline_5"],
            ),
        ]
    )
