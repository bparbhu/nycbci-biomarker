from pyedflib import highlevel
import pandas as pd
from google.cloud import bigtable
from google.cloud import bigquery
from google.cloud.bigtable import Client
import dask.dataframe as dd



def read_edf_store_bigtable(catalog: DataCatalog) -> pd.DataFrame:
    # Read EDF (Electroencephalogram Data Format) file using pyedflib
    signals, signal_headers, header = highlevel.read_edf('your_file.edf')
    # Convert to DataFrame
    df = pd.DataFrame(signals).T
    df.columns = [h['label'] for h in signal_headers]

    # Adding time and id for tsfresh
    df['time'] = df.index
    df['id'] = 'some_patient_id'

    # Storing into Google Cloud Bigtable
    client = Client(project='your_project_id')
    instance = client.instance('your_instance_id')
    table = instance.table('your_table_id')

    # Assume that 'time' is the row key
    for idx, row in df.iterrows():
        row_key = f'time_{row["time"]}'
        row_key = row_key.encode()

        # Assuming you have a single column family named 'cf1'
        column_family_id = 'cf1'

        direct_row = table.direct_row(row_key)
        for column in df.columns:
            value = str(row[column])
            direct_row.set_cell(column_family_id,
                                column,
                                value.encode('utf-8'))

        direct_row.commit()

    return df


# New function to read Parquet using Dask and upload to BigQuery
def dask_read_parquet_upload_bigquery(parquet_file_path: str, bigquery_project_id: str, dataset_name: str,
                                      table_name: str):
    # Read Parquet file using Dask
    dask_df = dd.read_parquet(parquet_file_path)

    # Convert to Pandas DataFrame
    pandas_df = dask_df.compute()

    # Initialize BigQuery client
    bigquery_client = bigquery.Client(project=bigquery_project_id)

    # Reference the BigQuery table
    dataset_ref = bigquery_client.dataset(dataset_name)
    table_ref = dataset_ref.table(table_name)

    # Upload Pandas DataFrame to BigQuery
    job_config = bigquery.LoadJobConfig()
    job = bigquery_client.load_table_from_dataframe(pandas_df, table_ref, job_config=job_config)

    job.result()  # Wait for the job to complete


def bigtable_to_dataframe_to_parquet(bigtable_project_id, bigtable_instance_id, table_name, parquet_file_path):
    # Initialize Bigtable client
    bigtable_client = bigtable.Client(project=bigtable_project_id)

    # Reference the Bigtable table
    instance = bigtable_client.instance(bigtable_instance_id)
    table = instance.table(table_name)

    # Scan Bigtable data
    rows = table.read_rows()
    rows.consume_all()

    # Transform Bigtable rows into a Pandas DataFrame
    data = []
    for row in rows:
        transformed_row = {
            'column_1': row.cells['family_name']['qualifier'][0].value.decode('utf-8'),
            # ... (transform other columns)
        }
        data.append(transformed_row)

    df = pd.DataFrame(data)

    # Write DataFrame to a Parquet file
    df.to_parquet(parquet_file_path)


def upload_parquet_to_bigquery(parquet_file_path, bigquery_project_id, dataset_name, table_name):
    # Initialize BigQuery client
    bigquery_client = bigquery.Client(project=bigquery_project_id)

    # Reference the BigQuery table
    dataset_ref = bigquery_client.dataset(dataset_name)
    table_ref = dataset_ref.table(table_name)

    # Create a job to upload the Parquet file to BigQuery
    job_config = bigquery.LoadJobConfig(source_format=bigquery.SourceFormat.PARQUET)
    with open(parquet_file_path, 'rb') as parquet_file:
        job = bigquery_client.load_table_from_file(parquet_file, table_ref, job_config=job_config)

    job.result()  # Wait for the job to complete
