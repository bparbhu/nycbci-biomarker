from kedro.pipeline import Pipeline, node
from kedro.framework.hooks import hook_impl

from src.kedro_streamz.pipelines.preprocessing import nodes as preprocess_node
from src.kedro_streamz.pipelines.tsfresh_features import nodes as tsfresh_node



class ProjectHooks:

    @hook_impl
    def register_pipelines(self) -> Dict[str, Pipeline]:
        pre_processing_pipeline = Pipeline(
            [
                node(
                    preprocess_node.read_edf_store_bigtable,
                    "raw_edf_file",
                    "intermediate_df",
                    name="read_edf_store_bigtable"
                ),
                node(
                    preprocess_node.dask_read_parquet_upload_bigquery,
                    ["parquet_file_path", "bigquery_project_id", "dataset_name", "table_name"],
                    None,
                    name="dask_read_parquet_upload_bigquery"
                ),
                node(
                    preprocess_node.bigtable_to_dataframe_to_parquet,
                    ["bigtable_project_id", "bigtable_instance_id", "table_name", "parquet_file_path"],
                    None,
                    name="bigtable_to_dataframe_to_parquet"
                ),
                node(
                    preprocess_node.upload_parquet_to_bigquery,
                    ["parquet_file_path", "bigquery_project_id", "dataset_name", "table_name"],
                    None,
                    name="upload_parquet_to_bigquery"
                ),
            ],
            name="pre_processing_pipeline"
        )

        ts_fresh_pipeline = Pipeline(
            [
                node(
                    tsfresh_node.extract_time_series_features,
                    "catalog",
                    "features_df",
                    name="extract_time_series_features"
                ),
                node(
                    tsfresh_node.boruta_feature_selection_and_store,
                    ["features_df", "target_variable"],
                    "selected_features",
                    name="boruta_feature_selection_and_store"
                ),
                node(
                    tsfresh_node.store_features_bigquery,
                    "features_df",
                    None,
                    name="store_features_bigquery"
                ),
                node(
                    tsfresh_node.store_features_dask_parquet,
                    ["features_df", "parquet_path"],
                    None,
                    name="store_features_dask_parquet"
                ),
                node(
                    tsfresh_node.dask_parquet_to_bigquery,
                    ["parquet_path", "bigquery_project_id", "dataset_name", "table_name"],
                    None,
                    name="dask_parquet_to_bigquery"
                ),
            ],
            name="ts_fresh_pipeline"
        )

        # Combine pipelines
        de_pipeline = pre_processing_pipeline + ts_fresh_pipeline

        return {"de": de_pipeline, "pre_processing": pre_processing_pipeline, "ts_fresh": ts_fresh_pipeline}
