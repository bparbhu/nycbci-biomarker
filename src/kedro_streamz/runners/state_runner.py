from kedro.runner import SequentialRunner
from src.kedro_streamz.runners.dask_runner import DaskRunner  # Assuming you've made a DaskRunner
from src.kedro_streamz.runners.streamz_runner import StreamzRunner  # Assuming you've made a StreamzRunner

class StateRunner:
    def run(self, pipeline, catalog, run_id=None):
        # Load the pipeline state
        state = catalog.load("pipeline_state")

        if state == 'use_subpipeline_1':
            runner = SequentialRunner()
            sub_pipeline = pipeline.only_nodes_with_tags("subpipeline_1")
        elif state == 'use_subpipeline_2':
            runner = DaskRunner()
            sub_pipeline = pipeline.only_nodes_with_tags("subpipeline_2")
        elif state == 'use_subpipeline_3':
            runner = StreamzRunner()
            sub_pipeline = pipeline.only_nodes_with_tags("subpipeline_3")

        runner.run(sub_pipeline, catalog, run_id)
