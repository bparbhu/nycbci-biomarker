from src.kedro_streamz.runners.state_runner import StateRunner

class ProjectContext(KedroContext):
    ...
    def run(self, tags: Iterable[str] = None, runner: AbstractRunner = None, *args, **kwargs):
        """Run the pipeline."""
        # load the pipeline
        pipeline = self.pipeline

        if runner is None:
            runner = StateRunner()

        return runner.run(pipeline, self.catalog, *args, **kwargs)
