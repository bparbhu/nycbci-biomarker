from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from kedro.framework.context import KedroContext
from kedro.framework.startup import bootstrap_project
from google.cloud import bigquery

class PipelineTrigger(BaseModel):
    pipeline_name: str

app = FastAPI()

# Initialize BigQuery client
client = bigquery.Client()

# Create BigQuery table if not exists
dataset_name = "your_dataset_name"
table_name = "pipeline_states"
table_id = f"{client.project}.{dataset_name}.{table_name}"
schema = [
    bigquery.SchemaField("id", "INTEGER", mode="REQUIRED"),
    bigquery.SchemaField("state", "STRING", mode="REQUIRED"),
]
client.create_table(bigquery.Table(table_id, schema=schema), exists_ok=True)

@app.post("/trigger_pipeline/")
async def trigger_pipeline(payload: PipelineTrigger):
    try:
        pipeline_name = payload.pipeline_name

        # Load Kedro context
        context = KedroContext(package_name="nycbci-biomarker")
        bootstrap_context = bootstrap_project(context)
        context.bootstrap(bootstrap_context)

        # Trigger the pipeline
        context.run(pipeline_name=pipeline_name)

        # Update pipeline state in BigQuery
        query = f"INSERT INTO `{table_id}` (id, state) VALUES (NULL, 'COMPLETED')"
        query_job = client.query(query)
        query_job.result()

        return {"status": "success", "message": f"Pipeline {pipeline_name} triggered successfully.", "state": "COMPLETED"}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

@app.get("/get_pipeline_state/")
async def get_pipeline_state():
    try:
        query = f"SELECT state FROM `{table_id}` ORDER BY id DESC LIMIT 1"
        query_job = client.query(query)
        results = query_job.result()
        for row in results:
            return {"state": row.state}
        return {"state": "UNKNOWN"}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
