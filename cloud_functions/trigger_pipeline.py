from google.cloud import storage
import subprocess
import os

def trigger_pipeline(event, context):
    """Trigger a Kedro pipeline when a new file is uploaded to GCP Storage."""
    # Initialize GCP Storage client
    storage_client = storage.Client()

    # Extract bucket and file info from the event
    bucket_name = event['bucket']
    file_name = event['name']

    # Your logic to run Kedro pipeline, assuming Kedro is installed on the same environment
    if file_name.endswith('.edf'):  # You can specify other conditions
        print(f"New EDF file {file_name} uploaded to {bucket_name}, triggering pipeline...")
        try:
            # Trigger your Kedro pipeline. Change the command as needed.
            subprocess.run(["kedro", "run", "--pipeline", "your_pipeline_name"], check=True)
        except subprocess.CalledProcessError as e:
            print(f"Failed to run Kedro pipeline: {e}")
