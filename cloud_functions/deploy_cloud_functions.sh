#!/bin/bash

# Set environment variables
PROJECT_ID="your_project_id"
REGION="your_region"
FUNCTION_NAME="trigger_pipeline"
SOURCE_DIR="./cloud_functions"
RUNTIME="python310" # or other versions you may use
ENTRY_POINT="trigger_pipeline"

# Deploy the trigger_pipeline function
gcloud functions deploy $FUNCTION_NAME \
    --project $PROJECT_ID \
    --region $REGION \
    --runtime $RUNTIME \
    --source $SOURCE_DIR \
    --entry-point $ENTRY_POINT \
    --trigger-resource your_trigger_resource \
    --trigger-event your_trigger_event

# Add additional functions to deploy as needed

# Don't forget to add any necessary environment variables, service accounts, or VPC connectors.
