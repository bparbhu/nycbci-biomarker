provider "google" {
  credentials = file("path/to/your-service-account-key.json")
  project     = var.project_id
  region      = "us-central1"
}

resource "google_bigquery_dataset" "eeg_dataset" {
  dataset_id  = "eeg_dataset"
  location    = "US"
}

resource "google_bigquery_table" "raw_eeg_data" {
  name     = "raw_eeg_data"
  dataset_id = google_bigquery_dataset.eeg_dataset.dataset_id
  schema = file("schemas/raw_eeg_data_schema.json")
}

resource "google_bigquery_table" "processed_eeg_data" {
  name     = "processed_eeg_data"
  dataset_id = google_bigquery_dataset.eeg_dataset.dataset_id
  schema = file("schemas/processed_eeg_data_schema.json")
}

resource "google_bigquery_table" "eeg_features_table" {
  name     = "eeg_features_table"
  dataset_id = google_bigquery_dataset.eeg_dataset.dataset_id
  schema = file("schemas/eeg_features_table_schema.json")
}
