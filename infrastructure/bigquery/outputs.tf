output "eeg_dataset_id" {
  value = google_bigquery_dataset.eeg_dataset.dataset_id
}

output "raw_eeg_data_table_id" {
  value = google_bigquery_table.raw_eeg_data.id
}

output "processed_eeg_data_table_id" {
  value = google_bigquery_table.processed_eeg_data.id
}

output "eeg_features_table_id" {
  value = google_bigquery_table.eeg_features_table.id
}
