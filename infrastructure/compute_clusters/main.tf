provider "kubernetes" {
  config_path = "~/.kube/config"  # Replace with your own Kubeconfig path
}

# Dask Scheduler Deployment
resource "kubernetes_deployment" "dask-scheduler" {
  metadata {
    name = "dask-scheduler"
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "dask-scheduler"
      }
    }
    template {
      metadata {
        labels = {
          app = "dask-scheduler"
        }
      }
      spec {
        container {
          image = "daskdev/dask:latest"
          name  = "dask-scheduler"
          args  = ["dask-scheduler"]
        }
      }
    }
  }
}

# Dask Worker Deployment
resource "kubernetes_deployment" "dask-worker" {
  metadata {
    name = "dask-worker"
  }
  spec {
    replicas = 3
    selector {
      match_labels = {
        app = "dask-worker"
      }
    }
    template {
      metadata {
        labels = {
          app = "dask-worker"
        }
      }
      spec {
        container {
          image = "daskdev/dask:latest"
          name  = "dask-worker"
          args  = ["dask-worker", "$(DASK_SCHEDULER_SERVICE_HOST):$(DASK_SCHEDULER_SERVICE_PORT)"]
        }
      }
    }
  }
}
