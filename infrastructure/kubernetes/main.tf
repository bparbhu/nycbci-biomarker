provider "google" {
  credentials = file("path/to/your-service-account-key.json")
  project     = var.project_id
  region      = "us-central1"
}

resource "google_container_cluster" "streamlit_cluster" {
  name               = "streamlit-cluster"
  location           = "us-central1-a"
  initial_node_count = 1

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }

  node_config {
    preemptible  = true
    machine_type = "n1-standard-1"

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}
