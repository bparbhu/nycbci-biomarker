output "cluster_name" {
  value = google_container_cluster.streamlit_cluster.name
}

output "cluster_location" {
  value = google_container_cluster.streamlit_cluster.location
}
