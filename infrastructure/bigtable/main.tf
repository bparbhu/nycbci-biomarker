provider "google" {
  credentials = file("path/to/your-service-account-key.json")
  project     = var.project_id
  region      = "us-central1"
}

resource "google_bigtable_instance" "eeg_bigtable_instance" {
  name         = "eeg-bigtable-instance"
  cluster_id   = "eeg-cluster"
  zone         = "us-central1-b"
  num_nodes    = 1
  storage_type = "SSD"
}

resource "google_bigtable_table" "eeg_bigtable" {
  name          = "eeg-table"
  instance_name = google_bigtable_instance.eeg_bigtable_instance.name
  column_family {
    family = "cf1"
  }
  column_family {
    family = "cf2"
  }
}
