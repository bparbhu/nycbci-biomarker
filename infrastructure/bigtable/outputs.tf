output "bigtable_instance_name" {
  value = google_bigtable_instance.eeg_bigtable_instance.name
}

output "bigtable_table_name" {
  value = google_bigtable_table.eeg_bigtable.name
}
