variable "onnx_runtime_replicas" {
  description = "Number of replicas for ONNX Runtime Deployment"
  type        = number
  default     = 1
}

variable "onnx_runtime_image" {
  description = "Docker image for ONNX Runtime"
  type        = string
  default     = "mcr.microsoft.com/onnxruntime/server:latest"
}

variable "onnx_runtime_container_port" {
  description = "Container port for ONNX Runtime"
  type        = number
  default     = 8001
}

variable "onnx_runtime_service_port" {
  description = "Service port for ONNX Runtime"
  type        = number
  default     = 8001
}
