# ONNX Runtime Deployment
resource "kubernetes_deployment" "onnx-runtime" {
  metadata {
    name = "onnx-runtime"
  }
  spec {
    replicas = var.onnx_runtime_replicas
    selector {
      match_labels = {
        app = "onnx-runtime"
      }
    }
    template {
      metadata {
        labels = {
          app = "onnx-runtime"
        }
      }
      spec {
        container {
          image = var.onnx_runtime_image
          name  = "onnx-runtime"
          ports {
            container_port = var.onnx_runtime_container_port
          }
        }
      }
    }
  }
}

# ONNX Runtime Service
resource "kubernetes_service" "onnx-runtime-service" {
  metadata {
    name = "onnx-runtime-service"
  }
  spec {
    selector = {
      app = kubernetes_deployment.onnx-runtime.metadata[0].labels.app
    }
    port {
      port        = var.onnx_runtime_service_port
      target_port = var.onnx_runtime_container_port
    }
  }
}
