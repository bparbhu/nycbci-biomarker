import streamlit as st
from kedro.framework.context import KedroContext
from kedro.framework.hooks import _create_hook_manager
from kedro.framework.startup import _add_src_to_path
from pathlib import Path

_project_path = Path.cwd()  # Assuming your Streamlit app is in the Kedro project root
_add_src_to_path(source_dir=_project_path / "src", project_path=_project_path)
context = KedroContext(package_name="nycbci-biomarker",
                       project_path=_project_path,
                       hook_manager=_create_hook_manager())


# Function to handle file upload
def file_upload():
    uploaded_file = st.file_uploader("Upload an EDF file", type=['edf'])
    if uploaded_file is not None:
        st.write("File successfully uploaded.")

        # Trigger Kedro pipeline for file loading and database import
        context.run(pipeline_name="de_load_and_import", params={"uploaded_file": uploaded_file})
        st.write("Data successfully loaded and imported into the database.")


# Function to visualize EEG data
def eeg_visualization():
    st.write("EEG Visualizations go here.")


# Function to show data quality metrics
def data_quality():
    st.write("Data Quality metrics go here.")


# Main App
def main():
    st.title("EEG Data Platform")

    # Create tabs
    tab = st.selectbox("Choose a tab", ["File Upload", "EEG Visualization", "Data Quality"])

    # Handle tabs
    if tab == "File Upload":
        file_upload()
    elif tab == "EEG Visualization":
        eeg_visualization()
    elif tab == "Data Quality":
        data_quality()


if __name__ == "__main__":
    main()
